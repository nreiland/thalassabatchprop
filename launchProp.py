"""Script to launch brute force conjunction analysis using the THALASSA orbitl Propagator"""

# Import Packages
import os
import shutil
import sys

# Import Functions
from genThalassaInputs import genThalassaList
from batchProp import thalassaBatchProp

# Import Settings
import settings as s


def main():
    """main function to be mapped to the command line"""

    # Create output directory tree
    outputDirs = createOutputDir(s.outputDir)

    # create batch thalassa propagation configuration inputs
    genBatchPropConfig(outputDirs)

    # Create thalassa input list
    genThalassaList(s.objectsPath, outputDirs['input'], s.objectNames)
    
    # Propagate objects
    thalassaBatchProp(s.thalassaDir, '{}/settings.json'.format(outputDirs['input']), '{}/{}.txt'.format(outputDirs['input'], s.objectNames))
    
    return
        
def formatOutputDir(outputDir):
    """Function to format output directory string"""

    if outputDir.endswith('/'):
        return outputDir[:-1]
    
    return outputDir

def createOutputDir(outputDir):
    """ Function to create output directory structure"""

    # format output directory pth
    outputDir = formatOutputDir(outputDir)

    # make directory structure
    makeDir(outputDir)
    os.makedirs('{}/input'.format(outputDir))
    os.makedirs('{}/output'.format(outputDir))
    os.makedirs('{}/grid'.format(outputDir))

    # define output directory
    output = {
        'input' : '{}/input'.format(outputDir),
        'output' : '{}/output'.format(outputDir),
        'grid' : '{}/grid'.format(outputDir),
    }
    return output

def makeDir(directoryPath):
    """Function to delete and recreate directories, "clean", directories"""

    # check if directory exists
    if os.path.isdir(directoryPath) is True:
        # delete directory
        shutil.rmtree(directoryPath)
        # replace directory
        os.makedirs(directoryPath)
    else:
        # create directory
        os.makedirs(directoryPath)	
    return

def genBatchPropConfig(outputDir):
    """Function to create configuration files for the THALASSA batch propagator"""

    def createBatchConfig(propPath, gridPath, objName):
        """Function to create THALASSA json settings dictionary"""

        # create settings dictionary
        settings = {}
        # output paths
        settings["outputPath"] = {
            "propPath": propPath
        }
        # physical model
        settings["physicalModel"] = {
            "insgrav": s.insgrav,
            "isun": s.isun,
            "imoon": s.imoon,
            "idrag": s.idrag,
            "iF107": s.iF107,
            "iSRP": s.iSRP,
            "iephem": s.iephem,
            "gdeg": s.gdeg,
            "gord": s.gord
        }
        # integration
        settings["integration"] = {
            "tol": s.tol,
            "tspan": s.tspan,
            "tstep": s.tstep,
            "mxstep": s.mxstep
        }
        # equations of motion
        settings["eOfM"] = {
            "eqs": s.eqs
        }
        # grid
        settings["grid"] = {
            "gridPath": gridPath,
        }
        # output
        settings["output"] = {
            "verb": s.verb,
            "objName": objName,
        }
        return settings

    def printConfig(settings, path):
        """Function to print settings dictionary to json file"""

        # Import packages
        import json

        # open file and print
        with open(path, "w") as fp:
            json.dump(settings, fp, indent = 2)

    # create settings dictionary
    settings = createBatchConfig(outputDir['output'], outputDir['grid'], s.objectNames)

    # print settings dictionaries
    printConfig(settings, '{}/settings.json'.format(outputDir['input']))
     

    return 

# map command line arguments to function arguments
if __name__ == '__main__':

	main(*sys.argv[1:])
